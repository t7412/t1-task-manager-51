package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectTaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public ProjectTaskBindToProjectRequest(@Nullable final String token) {
        super(token);
    }

}
